 // ფუნქცია შეზღუდოს შეყვანა რიცხვითი მნიშვნელობებით
 function allowOnlyNumbers(event) {
    const keyCode = event.keyCode || event.which;
    const allowedKeys = [8, 9, 37, 39, 46]; // Backspace, Tab, მარცხენა ისარი, მარჯვენა ისარი, წაშლა
    if (allowedKeys.includes(keyCode) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  document.getElementById('mobileNumber').addEventListener('input', function (e) {
    // ამოიღეთ არაციფრული სიმბოლოები შეყვანიდან
    this.value = this.value.replace(/\D/g, '');
  });

  // მიამაგრეთ allowOnlyNumbers ფუნქცია მობილური mobileNumber field მოვლენას
  document.getElementById('mobileNumber').addEventListener('keypress', allowOnlyNumbers);

  function validateForm() {
    var email = document.getElementById('email').value;

    // შეამოწმეთ, შეიცავს თუ არა ელფოსტა "@" სიმბოლოს
    if (email.indexOf('@') === -1) {
      displayError('Email must contain the "@" symbol');
      return;
    }

    // შეამოწმეთ არის თუ არა წერტილი (.) სიმბოლო "@" სიმბოლოს შემდეგ
    var dotIndex = email.indexOf('.', email.indexOf('@'));
    if (dotIndex === -1) {
      displayError('Invalid email address. Missing dot (.) after "@"');
      return;
    }

    // შეამოწმეთ, აქვს თუ არა ელფოსტას სწორი ფორმატი ("@" სიმბოლოს შემდეგ, მინიმუმ ერთი სიმბოლო, რასაც მოჰყვება მინიმუმ ორი სიმბოლო ".")
    var atSymbolIndex = email.indexOf('@');
    var dotAfterAt = email.lastIndexOf('.') - atSymbolIndex;

    if (atSymbolIndex < 1 || dotAfterAt < 2) {
      displayError('Invalid email address');
      return;
    }

    // შეამოწმეთ პაროლის სიძლიერე
    var password = document.getElementById('password').value;
    var strengthMessage = getPasswordStrengthMessage(password);
    var strengthBarClasses = getPasswordStrengthBarClasses(password);

    document.getElementById('passwordStrength').innerHTML = strengthMessage;

    var passwordStrengthBar = document.getElementById('passwordStrengthBar');
    passwordStrengthBar.style.display = 'block'; // აჩვენე ზოლი
    passwordStrengthBar.children[0].className = 'bar-part ' + strengthBarClasses[0];
    passwordStrengthBar.children[1].className = 'bar-part ' + strengthBarClasses[1];
    passwordStrengthBar.children[2].className = 'bar-part ' + strengthBarClasses[2];

    // შეამოწმეთ ემთხვევა თუ არა პაროლები
    if (!checkPasswordMatch()) {
      return;
    }

    // შეამოწმეთ, თუ სახელი, ქვეყანა და ქალაქი ცარიელი არ არის
    var name = document.getElementById('name').value;
    var country = document.getElementById('country').value;
    var city = document.getElementById('city').value;

    if (name.trim() === '' || country.trim() === '' || city.trim() === '') {
      displayError('Name, Country, and City cannot be empty');
      return;
    }

    // შეამოწმეთ არის თუ არა მინიმუმ ერთი რიცხვითი სიმბოლო მობილურის ნომერში
    var mobileNumber = document.getElementById('mobileNumber').value;
    if (!/\d/.test(mobileNumber)) {
      displayError('Mobile Number must contain at least one numeric character');
      return;
    }

    // თუ ყველა ვალიდაცია გაივლის, გაგზავნეთ ფორმა (შეგიძლიათ შეცვალოთ ის AJAX ზარით)
    alert('Registration successful!');
  }

  function displayError(message) {
    var errorDiv = document.createElement('div');
    errorDiv.className = 'error';
    errorDiv.appendChild(document.createTextNode(message));

    var form = document.getElementById('registrationForm');
    form.appendChild(errorDiv);
  }

  function checkPasswordStrength() {
    var password = document.getElementById('password').value;

    // პაროლის სიძლიერის ზოლის ჩვენება პაროლის ველში აკრეფისას
    var passwordStrengthBar = document.getElementById('passwordStrengthBar');
    passwordStrengthBar.style.display = 'block';

    var strengthMessage = getPasswordStrengthMessage(password);
    var strengthBarClasses = getPasswordStrengthBarClasses(password);

    document.getElementById('passwordStrength').innerHTML = strengthMessage;

    var passwordStrengthBar = document.getElementById('passwordStrengthBar');
    passwordStrengthBar.children[0].className = 'bar-part ' + strengthBarClasses[0];
    passwordStrengthBar.children[1].className = 'bar-part ' + strengthBarClasses[1];
    passwordStrengthBar.children[2].className = 'bar-part ' + strengthBarClasses[2];
  }

  function getPasswordStrengthMessage(password) {
    var hasSymbol = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]/.test(password);
    var hasDigit = /\d/.test(password);

    var characterTypes = [hasSymbol, hasDigit].filter(Boolean).length;

    if (characterTypes === 2) {
      return 'Password strength: Strong (contains at least one symbol and one number)';
    } else if (characterTypes === 1) {
      return 'Password strength: Average (contains at least one type of character: symbol or number)';
    } else {
      return 'Password strength: Weak (contains only letters)';
    }
  }

  function getPasswordStrengthBarClasses(password) {
    var hasSymbol = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]/.test(password);
    var hasDigit = /\d/.test(password);

    var characterTypes = [hasSymbol, hasDigit].filter(Boolean).length;

    if (characterTypes === 2) {
      return ['weak-bar', 'average-bar', 'strong-bar'];
    } else if (characterTypes === 1) {
      return ['weak-bar', 'average-bar', ''];
    } else {
      return ['weak-bar', '', ''];
    }
  }

  function checkPasswordMatch() {
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;
    var passwordMatchError = document.getElementById('passwordMatchError');

    if (password !== confirmPassword) {
      passwordMatchError.innerHTML = 'Passwords do not match';
      return false;
    } else {
      passwordMatchError.innerHTML = '';
      return true;
    }
  }