function changeLanguage() {
    var select = document.getElementById("language");
    var selectedLanguage = select.options[select.selectedIndex].value;
    
  
    switch (selectedLanguage) {
      case "en":
        document.getElementById("homepage").innerText = "Home";
        document.getElementById("searchpage").innerText = "Search";
        document.getElementById("libpage").innerText = "Library";
        document.getElementById("creatplayorfolder").innerText = "Create a playlist or a folder";
        document.getElementById("creatplay").innerText = "Create a new playlist";
        document.getElementById("creatfolder").innerText = "Creat a new playlist folder";
        document.getElementById("navlikedsong").innerText = "Liked songs";
        document.getElementById("navplaylist").innerText = "My playlist";
        document.getElementById("navplaylist1").innerText = "My playlist 2";
        document.getElementById("navplaylist2").innerText = "My playlist 3";
        document.getElementById("sel_lang").innerText = "Select Language:";
        document.getElementById("titprem").innerText = "Premium";
        document.getElementById("titsup").innerText = "Support";
        document.getElementById("titabout").innerText = "about";
        document.getElementById("titreg").innerText = "Register";
        document.getElementById("titlogin").innerText = "Log in";
        document.getElementById("wb").innerText = "Welcome!";
        document.getElementById("mllikedsong").innerText = "Liked songs";
        document.getElementById("jazzmix").innerText = "jazz mix";
        document.getElementById("christmasmix").innerText = "christmass mix";

        document.getElementById("mfu").innerText = "Made for you";
        document.getElementById("dm1").innerText = "Daily Mix 1";
        document.getElementById("dm2").innerText = "Daily Mix 2";
        document.getElementById("dm3").innerText = "Daily Mix 3";
        document.getElementById("dm4").innerText = "Daily Mix 4";
        document.getElementById("dm5").innerText = "Daily Mix 5";
        document.getElementById("rft").innerText = "Recommended for today";
        document.getElementById("searchButton").innerText = "Search";
        
        break;
      case "geo":
        document.getElementById("homepage").innerText = "მთავარი";
        document.getElementById("searchpage").innerText = "ძებნა";
        document.getElementById("libpage").innerText = "ბიბლიოთეკა";
        document.getElementById("creatplayorfolder").innerText = "შექმენით დასაკრავი სია ან საქაღალდე";
        document.getElementById("creatplay").innerText = "შექმენით ახალი დასაკრავი სია";
        document.getElementById("creatfolder").innerText = "შექმენით ახალი დასაკრავი სიის საქაღალდე";
        document.getElementById("navlikedsong").innerText = "მოწონებული სიმღერები";
        document.getElementById("navplaylist").innerText = "ჩემი დასაკრავი სია";
        document.getElementById("navplaylist1").innerText = "ჩემი დასაკრავი სია 2";
        document.getElementById("navplaylist2").innerText = "ჩემი დასაკრავი სია 3";
        document.getElementById("sel_lang").innerText = "აირჩიეთ ენა:";
        document.getElementById("titprem").innerText = "პრემიუმი";
        document.getElementById("titsup").innerText = "მხარდაჭერა";
        document.getElementById("titabout").innerText = "ინფორმაცია";
        document.getElementById("titreg").innerText = "რეგისტრაცია";
        document.getElementById("titlogin").innerText = "Შესვლა";
        document.getElementById("wb").innerText = "მოგესალმებით";
        document.getElementById("mllikedsong").innerText = "მოწონებული სიმღერები";
        document.getElementById("jazzmix").innerText = "ჯაზ მიქსი";
        document.getElementById("christmasmix").innerText = "საახალწლო მიქსი";
        document.getElementById("mfu").innerText = "შექმნილი თქვენთვის";
        document.getElementById("dm1").innerText = "დღიური მიქსი 1";
        document.getElementById("dm2").innerText = "დღიური მიქსი 2";
        document.getElementById("dm3").innerText = "დღიური მიქსი 3";
        document.getElementById("dm4").innerText = "დღიური მიქსი 4";
        document.getElementById("dm5").innerText = "დღიური მიქსი 5";
        document.getElementById("rft").innerText = "რეკომენდირებულია";
        document.getElementById("searchButton").innerText = "ძებნა";


        
      

        break;
     
    }
  }