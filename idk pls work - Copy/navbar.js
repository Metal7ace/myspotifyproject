document.addEventListener('DOMContentLoaded', function() {
    // Audio element
    const audio = new Audio('./mp3/when you sleep.mp3');

    // მომხმარებლის კონტროლი
    const playPauseButton = document.getElementById('play-pause-button');
    const playPauseIcon = document.getElementById('play-pause-icon');
    const progressBar = document.getElementById('progress-bar');
    const progressContainer = document.getElementById('progress-bar-container');
    const volumeSlider = document.getElementById('volume-slider');

    // ჩართვა გაჩერების ფუნქცია
    playPauseButton.addEventListener('click', function() {
        if (audio.paused) {
            audio.play();
            
        } else {
            audio.pause();
            
        }
        updatePlayPauseIcon();
        
    });

    // დააბდაითე პროგრესს ბარი როდესაც მუსიკა გრძელდება
    audio.addEventListener('timeupdate', function() {
        const percentage = (audio.currentTime / audio.duration) * 100;
        progressBar.style.width = `${percentage}%`;
    });

    // დასკიპე სიმღერის ნაწილები კილიკით
    progressContainer.addEventListener('click', function(event) {
        const progressBarRect = progressContainer.getBoundingClientRect();
        const clickPosition = event.clientX - progressBarRect.left;
        const percentage = (clickPosition / progressBarRect.width) * 100;
        const newTime = (percentage / 100) * audio.duration;
        audio.currentTime = newTime;
    });

    // ხმის შეცვლა
    volumeSlider.addEventListener('input', function() {
        audio.volume = volumeSlider.value / 100;
    });

    // ჩართვა გამორთვის იქონების შეცვლა ძან წვალება
function updatePlayPauseIcon() {
    playPauseIcon.classList.remove('bx-play', 'bx-pause');
    playPauseIcon.classList.add(audio.paused ? 'bx-play' : 'bx-pause');
}

   
   

});


function toggleDropdown() {
    var dropdownContent = document.getElementById("dropdownContent");
    dropdownContent.style.display = (dropdownContent.style.display === "block") ? "none" : "block";
}

// დროპდოუნის დახურვა როხა სხვაგან აჭერ
window.onclick = function(event) {
    var dropdownContent = document.getElementById("dropdownContent");
    if (event.target != dropdownContent && !event.target.matches('.icon')) {
        dropdownContent.style.display = "none";
    }
};

document.getElementById('searchButton').addEventListener('click', function() {
    var keywordMappings = {
        'main': 'main.html',
        'about': 'about.html',
        'support': 'support.html',
        'liked songs': 'liksong.html'
        //სერჩის მაპინგი
    };

    var enteredKeyword = document.getElementById('searchInput').value.toLowerCase();
    
    if (keywordMappings.hasOwnProperty(enteredKeyword)) {
        window.location.href = keywordMappings[enteredKeyword]; // მაპდ გვერდძე რო ჩაწერ რო გადაგიყვანოს
    } else {
        alert('Keyword not found. You can customize this message or add more conditions.');
    }
});


